package  test.schibsted

class RaterTest extends UnitTest {

  describe("mapping") {
    val rows = List("this is a simple row")
    val keys = List("row", "simple")

    describe(" when empty rows or Nil") {
      val rr = List()
      it("should return empty list") {
        val tr = new TestRater(keys)
        tr.mapper(rr).size should equal(0)
        tr.mapper(Nil).size should equal(0)
        tr.mapper(List("")).size should equal(0)
      }
    }

    describe(" when empty keys or Nil") {
      it("should return empty list") {
        val tr = new TestRater(List())
        tr.mapper(rows).size should equal(0)
        val tr2 = new TestRater(Nil)
        tr2.mapper(List("")).size should equal(0)
        val tr3 = new TestRater(List(""))
        tr3.mapper(Nil).size should equal(0)
      }
    }

    describe(" when rows and keys are empty or Nil") {
      it(" should return empty list") {
        val tr = new TestRater(Nil)
        tr.mapper(Nil).size should equal(0)
        tr.mapper(rows).size should equal(0)
        tr.mapper(List("")).size should equal(0)
      }
    }

    describe("with rows and keys ") {
      val keys = List("row", "simple")
      it(" should return a combination of all the keys found in the row") {
        val tr = new TestRater(keys)
        tr.mapper(rows).size should equal (2)
      }
      it(" no matching no mapping") {
        val tr = new TestRater(List("abc", "def","ghi"))
        tr.mapper(rows).size should equal (0)
      }

      it(" partial matching should not count") {
        val tr = new TestRater(List("thi","ro"))
        tr.mapper(rows).size should equal (0)
      }
    }
  }

  describe("Reducing ") {
    val mapVal = List(("val1", 1), ("val1", 1), ("val1", 1), ("val2", 1))

    describe(" property mapped values") {
      it(" getting a reduce result with 2 entities") {
        val tr = new TestRater(List())
        tr.reducer(mapVal).size should equal (2)
      }
    }

    describe(" empty or Nil map ") {
      it("should return an empty reduced list") {
        val mapv = List[(String, Int)]()
        val tr = new TestRater(List())
        tr.reducer(mapv).size should equal (0)
        tr.reducer(Nil).size should equal (0)
      }
    }
  }

  describe("Rating ") {
    val keys = List("k1", "k2")
    describe(" found keys in the file ") {
      it(" all the keys - file should get 100% rate ") {
        val tr = new TestRater(keys)
        val mrL = Map("k1" -> 2, "k2" -> 1)
        tr.rate(mrL) should equal (100)
      }
      it(" hakf of the keys - file should get 50% rate ") {
        val tr = new TestRater(keys)
        val mrL = Map("k1" -> 2)
        tr.rate(mrL) should equal (50)
      }
    }
    describe("no key found ") {
      it(" retunr rating 0") {
        val tr = new TestRater(keys)
        val mrL = Map[String, Int]()
        tr.rate(mrL) should equal (0)
      }
    }
  }
}