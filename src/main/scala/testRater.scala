package  test.schibsted


trait MapAndRate {
  def reducer(mapValues: List[(String, Int)]): Map[String, Int]
  def mapper(rows: List[String]): List[(String, Int)]
  def rate(mr: Map[String, Int]): Int
}


class TestRater(keys: List[String]) extends MapAndRate {
  // if I found all the keys I got a bonus of 100 and then I add all the occurrence of the keys
  def rate(mr: Map[String, Int]): Int = {
    val kl = keys.length
    if (mr.size > 0 && kl > 0) {
      val numK = mr.keys.toList.length
      if(numK == kl) 100
      else (100/kl) * numK
    } else 0
  }

  def reducer(mapValues: List[(String, Int)]): Map[String, Int] = {
    mapValues.groupBy(_._1). // Map[String, List[(String, Int)]]
      map(w => (w._1, w._2.map(_=>1).sum))  // reduce with a Map for every keyword found and the # of occurences
  }

  // List[Map[Int, Map[Int, String]]] = {
  //def mapKeywordInFile(rows: String => List[String])(keys: List[String])(implicit rater: List[(String, Int)] => Int): Int = {
  def mapper(rows: List[String]): List[(String, Int)] = {
    if(keys.nonEmpty) {
      for {
        r <- rows
        word <- r.split(" ").toList
        kw <- keys
        if word == kw
      } yield (word, 1)
    } else
      List[(String, Int)]()
  }
}
