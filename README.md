# SimpleSearch #

The exercise is to write a command line driven text search engine, usage being:

> scala scalaClassName pathToDirectoryContainingTextFiles

This should read all the text files in the given directory, building an in memory representation
of the files and their contents, and then give a command prompt at which interactive
searches can be performed.

An example session might look like:

```
$ scala test.SimpleSearch /foo/bar
14 files read in directory /foo/bar
search>
search> to be or not to be
filename1 : 100%
filename2 : 95%
search>
search> cats
no matches found
search> :quit
$
```

I.e. the search should take the words given on the command prompt and return a list of the
top 10 (max) matching filenames in rank order, giving the rank score against each match.

**Note:**

treat the above as an outline spec; you don’t need to exactly reproduce the above output.


## Ranking

* The rank score must be 100% if a file contains all the words
* It must be 0% if it contains none of the words
* It should be between 0 and 100 if it contains only some of the words ­ but the exact
* ranking formula is up to you to choose and implement

### Things to consider in your implementation

- What constitutes a word
- What constitutes two words being equal (and matching)
- Data structure design: the in memory representation to search against
- Ranking score design: start with something basic then iterate as time allows
- Testability

---

### Pre-requisites ###

* Java JDK (Oracle or OpenJDK) 1.8.x
* Scala 2.11.8+ (compiled with 2.12.x) (http://www.scala-lang.org/) 
* sbt 0.13.x (http://www.scala-sbt.org/)

## How do I get set up? ###

* From the project root just run `$ sbt`
* '> update` or`compile` if you want to compile before running
* `> run <path to check>`  (eg `> run /Users/user1/dir1`)
* follow the instruction of the document (insert the key to search for, or :quit to end the program)
* CTRL+D to exit the sbt
