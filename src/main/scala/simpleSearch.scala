package  test.schibsted

import scala.io._
import java.io.File

import scala.util.{Failure, Success, Try}


object simpleSearch {

  // read the provided file and return a List of all the rows
  private def readFile(fName: String): List[String] = {
    // def ctx(x: Int): Stream[Int] = x #:: ctx(x + 1)
    // val idx = ctx(1).iterator
    val src = Try(Source.fromFile(fName))
    src match {
      case Success(fs) => {
        try {
          val lines = fs.getLines()
          if (lines.nonEmpty)
            lines.toList
          else
            List()
        } finally fs.close
      }
      case _ => List()
    }
  }

  // get the list of the filename contained in a directory
  private def getListOfFiles(dirName: Option[String]): List[String] = {
    dirName match {
      case Some(dn) if dn.length > 0 =>
        Try(new File(dn)) match {
          case Success(x) =>
            (for {
              lf <- x.listFiles()
              if lf.isFile
            } yield lf.getName).toList
          case _ => List()
        }
      case _ => List()
    }
  }

  def testRater(fName: String, rater: MapAndRate): Int = {
    rater.rate(
      rater.reducer(
        rater.mapper(readFile(fName))))
  }

  // get the list of files and return the top 10 in rank
  def rankTopNFiles(nFiles: Int)(dirName: Option[String], keys: List[String]): List[(String, Int)] = {
    val rater = new TestRater(keys)
    val ranking = getListOfFiles(dirName).map(fl => (fl, testRater(s"${dirName.getOrElse("")}/$fl", rater)))
    ranking.
      filter(_._2 > 0).// remove the files with 0 matches
      sortWith(_._2 > _._2). // sort in descending order
      take(nFiles) // return the ordered list of the top N ranked files
  }

  def aPrinter(rList: List[(String, Int)]): Unit = {
    if (rList.isEmpty)
      println("No matches found")
    else
      rList.map(x => println(x._1 + " " + x._2 + "%"))
  }

  private def parseInput(inp: Option[String]): Option[String] = {
    inp match {
      case Some(x: String) if x.compareToIgnoreCase(":quit") != 0 => Some(x)
      case _ => None
    }
  }

  def myConsole(dir: Option[String])(printer: List[(String, Int)] => Unit): Unit = {
    println("Reading directory " + dir.getOrElse(""))
    print("search> ")
    val inp = Option(StdIn.readLine())
    parseInput(inp) match { // getting the keywords to search
      case Some(x) => {
        val keys = x.split(" ").toList
        printer(rankTopNFiles(10)(dir, keys))
        println("---------------")
        myConsole(dir)(printer)
      }
      case _ => ()
    }
  }

  def main(args: Array[String]): Unit = {
    println("Insert :quit to exit")
    val dirs = Try(args(0)).toOption
    myConsole(dirs)(aPrinter)
  }
}
