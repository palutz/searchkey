val word = """(\b[A-Za-z0-9])""".r
val ew = word.unanchored

"just a simple teting string with cippa lippa and other " match {
  case ew(" simple ") => "!!!"
  case _ => "OOpppsss"
}
val rr = """(\w+)""".r
val ss = List("cippa","lippa","Steo")
val ws = "just a simple texting string with cippa lippa and other cippa"

ws match {
  case rr("cippa") => "!!!"
  case _ => "damn"
}

val r1 = for {
  s <- ss
  w <- ws.split(" ")
  if w == s
} yield (s, 1)


val m = r1.groupBy(_._1)   // Map(String, List[String, Int])
//val mm = m.map(w => (w._1, w._2.map(_=>1).reduce(_+_))) //w._1 : String, w._2
val mm = m.map(w => (w._1, w._2.map(_=>1).sum)) //w._1 : String, w._2
mm.map(x => x._1).toList.length
mm.map(x => x._2).fold(0)(_+_)

mm.keys.toList.length
mm.values.fold(0)(_+_)


  //.reduce(_._2 + _._2)
